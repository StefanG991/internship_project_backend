import { IsString } from 'class-validator';
export class ExistingUserDto {
  @IsString()
  password: string;
  @IsString()
  email: string;
}
