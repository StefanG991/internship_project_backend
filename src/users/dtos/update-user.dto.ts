import { IsDateString, IsNumber, IsOptional, IsString } from 'class-validator';
export class UpdateUserDto {
  @IsOptional()
  @IsString()
  name: string;
  @IsOptional()
  @IsString()
  gender: string;
  @IsOptional()
  @IsDateString()
  dateOfBirth: Date;
  @IsOptional()
  @IsNumber()
  height: number;
}
