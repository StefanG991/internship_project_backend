import {
  Body,
  Controller,
  Delete,
  Get,
  HttpStatus,
  Param,
  Patch,
  UseGuards,
  Res,
} from '@nestjs/common';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { UpdateUserDto } from './dtos/update-user.dto';
import { UserInterface } from './interfaces/user.interface';
import { UsersService } from './users.service';

@UseGuards(JwtGuard)
@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async getAll(@Res() response): Promise<UserInterface[]> {
    const users = await this.userService.findAll();
    return response.status(HttpStatus.OK).json({
      users,
    });
  }

  @Get(':id')
  async getOne(
    @Res() response,
    @Param('id') id: string,
  ): Promise<UserInterface> {
    const user = await this.userService.findById(id);
    if (user) {
      return response.status(HttpStatus.OK).json({
        ...user,
      });
    } else {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'User with id does not exist' });
    }
  }

  @Delete(':id')
  async delete(@Res() response, @Param('id') id: string): Promise<any> {
    const deletedUser = await this.userService.delete(id);
    if (deletedUser) {
      return response.status(HttpStatus.NO_CONTENT).json({});
    } else {
      return response.status(HttpStatus.NOT_FOUND).json({
        message: 'User with id does not exist',
      });
    }
  }

  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateUserDto: UpdateUserDto,
  ): Promise<UserInterface> {
    const updatedUser = await this.userService.update(id, updateUserDto);
    if (updatedUser) {
      return response.status(HttpStatus.OK).json({
        ...updatedUser,
      });
    } else {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'User with id does not exist' });
    }
  }
}
