import { Injectable, NotFoundException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { User, UserDocument } from './schemas/user.schema';
import { CreateUserDto } from './dtos/create-user.dto';
import { UserInterface } from './interfaces/user.interface';
import { UpdateUserDto } from './dtos/update-user.dto';

@Injectable()
export class UsersService {
  constructor(
    @InjectModel(User.name) private readonly userModel: Model<UserDocument>,
  ) {}

  async findAll(): Promise<UserInterface[]> {
    return this.userModel.find({}, { password: 0, __v: 0 }).lean();
  }

  async create(user: CreateUserDto): Promise<UserInterface> {
    const newUser = new this.userModel({
      ...user,
    });
    return newUser.save();
  }

  async findByEmail(email: string): Promise<UserDocument> | null {
    const user = await this.userModel.findOne({ email }).exec();
    if (user) return user;
    else return null;
  }

  async findById(id: string): Promise<UserInterface> | null {
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
      const user = await this.userModel
        .findById(id, { password: 0, __v: 0 })
        .lean();
      if (user) {
        return user;
      } else {
        return null;
      }
    } else {
      return null;
    }
  }

  async delete(id: string): Promise<UserInterface> | null {
    const existingUser = await this.findById(id);
    if (!existingUser) {
      return null;
    } else {
      await this.userModel.deleteOne({ _id: id });
      return existingUser;
    }
  }

  update(
    id: string,
    updateUserDto: UpdateUserDto,
  ): Promise<UserInterface> | null {
    return this.userModel
      .findByIdAndUpdate(id, updateUserDto, {
        new: true,
      })
      .lean()
      .catch(() => {
        return null;
      });
  }
}
