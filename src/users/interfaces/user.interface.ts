import { Role } from './role.enum';

export interface UserInterface {
  _id: string;
  password?: string;
  name: string;
  email: string;
  role: Role;
  gender: string;
  dateOfBirth: Date;
  height: number;
}
