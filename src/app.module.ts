import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { ConfigModule } from '@nestjs/config';
import { MeasurementsModule } from './measurements/measurements.module';
import { ExerciseTypeModule } from './exercise-type/exercise-type.module';
import { ExercisesModule } from './exercise/exercise.module';
import { WorkoutModule } from './workout/workout.module';

@Module({
  imports: [
    ConfigModule.forRoot(),
    UsersModule,
    MongooseModule.forRoot(
      `mongodb+srv://Stefan:${process.env.DATABASE_PASS}@primeinternship.weufz.mongodb.net/${process.env.DATABASE_NAME}?retryWrites=true&w=majority`,
    ),
    AuthModule,
    MeasurementsModule,
    ExerciseTypeModule,
    ExercisesModule,
    WorkoutModule,
  ],
})
export class AppModule {}
