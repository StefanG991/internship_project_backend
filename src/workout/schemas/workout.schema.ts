import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { Exercise } from 'src/exercise/schemas/exercise.schema';
import { User } from 'src/users/schemas/user.schema';

export type WorkoutDocument = Workout & Document;

@Schema()
export class Workout {
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;
  @Prop({ required: true })
  date: Date;
  @Prop({
    required: true,
    type: [mongoose.Schema.Types.ObjectId],
    ref: 'Exercise',
  })
  exercises: Array<Exercise>;
}

export const WorkoutSchema = SchemaFactory.createForClass(Workout);
