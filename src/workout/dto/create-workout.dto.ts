import { IsDateString, IsArray, IsString } from 'class-validator';

export class CreateWorkoutDto {
  @IsString()
  user: string;
  @IsArray()
  exercises: string[];
  @IsDateString()
  date: Date;
}
