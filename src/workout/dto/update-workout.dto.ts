import { IsDateString, IsArray, IsString, IsOptional } from 'class-validator';

export class UpdateWorkoutDto {
  @IsOptional()
  @IsString()
  user: string;
  @IsOptional()
  @IsArray()
  exercises: string[];
  @IsOptional()
  @IsDateString()
  date: Date;
}
