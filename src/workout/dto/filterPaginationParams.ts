import { IsNumber, Min, IsOptional, IsString, IsArray } from 'class-validator';
import { Transform, Type } from 'class-transformer';

export class FilterPaginationParams {
  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(0)
  skip?: number;

  @IsOptional()
  @Type(() => Number)
  @IsNumber()
  @Min(1)
  limit?: number;

  @IsOptional()
  @Type(() => String)
  @IsString()
  exerciseType?: string;

  @IsOptional()
  @Type(() => String)
  @IsString()
  start?: string;

  @IsOptional()
  @Type(() => String)
  @IsString()
  end?: string;

  @IsOptional()
  @Type(() => String)
  @Transform(({ value }) => {
    if (value) {
      return value.toString().split(',');
    } else {
      return value;
    }
  })
  muscleGroups?: string[];
}
