import { Exercise } from 'src/exercise/schemas/exercise.schema';
import { User } from 'src/users/schemas/user.schema';

export interface WorkoutInterface {
  _id: string;
  user: User;
  date: Date;
  exercises: Exercise[];
}
