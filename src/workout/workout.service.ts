import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import mongoose, { Model } from 'mongoose';
import { ExerciseTypeEnum } from 'src/exercise-type/interfaces/exercise-type.enum';
import { CreateWorkoutDto } from './dto/create-workout.dto';
import { UpdateWorkoutDto } from './dto/update-workout.dto';
import { WorkoutPaginationInterface } from './interfaces/workout-pagination.interface';
import { WorkoutInterface } from './interfaces/workout.interface';
import { Workout, WorkoutDocument } from './schemas/workout.schema';

@Injectable()
export class WorkoutService {
  constructor(
    @InjectModel(Workout.name)
    private readonly workoutModel: Model<WorkoutDocument>,
  ) {}

  async create(createWorkoutDto: CreateWorkoutDto): Promise<WorkoutInterface> {
    const workout = new this.workoutModel({ ...createWorkoutDto });
    return workout.save();
  }

  async findAll(): Promise<WorkoutInterface[]> {
    return this.workoutModel.find().lean();
  }

  async findOne(id: string): Promise<WorkoutInterface> | null {
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
      const workout = await this.workoutModel.findById(id).lean();
      if (!workout) {
        return null;
      } else {
        return workout;
      }
    } else {
      return null;
    }
  }

  async findByUser(
    userId: string,
    skip = 1,
    limit: number,
    exerciseType?: string,
    muscleGroups?: string[],
    start?: string,
    end?: string,
  ): Promise<WorkoutPaginationInterface> | null {
    if (userId.match(/^[0-9a-fA-F]{24}$/)) {
      start = start ? start : '2013-10-01T00:00:00.000Z';
      muscleGroups = muscleGroups
        ? muscleGroups
        : Object.values(ExerciseTypeEnum);
      exerciseType = exerciseType ? exerciseType : '';
      let workouts;
      if (end) {
        const arrayFromEnd = end.split('');
        arrayFromEnd.splice(18, 0, '9');
        end = arrayFromEnd.join('');
      }
      if (limit) {
        skip = skip < 1 ? 1 : skip;
        limit = limit > 50 ? 50 : limit;

        workouts = await this.workoutModel.aggregate([
          {
            $match: {
              $and: [
                { user: new mongoose.Types.ObjectId(userId) },
                {
                  date: {
                    $gte: new Date(start),
                    $lt: end ? new Date(end) : new Date(),
                  },
                },
              ],
            },
          },
          { $unwind: '$exercises' },
          {
            $lookup: {
              from: 'exercises',
              localField: 'exercises',
              foreignField: '_id',
              as: 'exercises',
            },
          },
          { $unwind: '$exercises' },
          {
            $lookup: {
              from: 'exercisetypes',
              localField: 'exercises.exerciseType',
              foreignField: '_id',
              as: 'exercises.exerciseType',
            },
          },
          { $unwind: '$exercises.exerciseType' },
          {
            $group: {
              _id: '$_id',
              date: { $first: '$date' },
              exercises: { $push: '$exercises' },
            },
          },
          { $sort: { date: -1 } },
          {
            $match: {
              'exercises.exerciseType.name': new RegExp(exerciseType, 'g'),
              'exercises.exerciseType.muscleGroups': { $in: muscleGroups },
            },
          },
          {
            $facet: {
              workouts: [{ $skip: (skip - 1) * limit }, { $limit: limit }],
              totalCount: [
                {
                  $count: 'count',
                },
              ],
            },
          },
        ]);

        return {
          workouts: workouts[0].workouts,
          numOfWorkouts: workouts[0].totalCount[0]?.count,
        };
      } else {
        workouts = await this.workoutModel.aggregate([
          {
            $match: {
              $and: [
                { user: new mongoose.Types.ObjectId(userId) },
                {
                  date: {
                    $gte: new Date(start),
                    $lt: end ? new Date(end) : new Date(),
                  },
                },
              ],
            },
          },
          { $unwind: '$exercises' },
          {
            $lookup: {
              from: 'exercises',
              localField: 'exercises',
              foreignField: '_id',
              as: 'exercises',
            },
          },
          { $unwind: '$exercises' },
          {
            $lookup: {
              from: 'exercisetypes',
              localField: 'exercises.exerciseType',
              foreignField: '_id',
              as: 'exercises.exerciseType',
            },
          },
          { $unwind: '$exercises.exerciseType' },
          {
            $group: {
              _id: '$_id',
              date: { $first: '$date' },
              exercises: { $push: '$exercises' },
            },
          },
          { $sort: { date: 1 } },
          {
            $match: {
              'exercises.exerciseType.name': new RegExp(exerciseType, 'g'),
            },
          },
          {
            $facet: {
              workouts: [{ $skip: 0 }],
              totalCount: [
                {
                  $count: 'count',
                },
              ],
            },
          },
        ]);
        return {
          workouts: workouts[0].workouts,
          numOfWorkouts: workouts[0].totalCount[0]?.count,
        };
      }
    } else {
      return null;
    }
  }

  update(
    id: string,
    updateWorkoutDto: UpdateWorkoutDto,
  ): Promise<WorkoutInterface> | null {
    return this.workoutModel
      .findByIdAndUpdate(id, updateWorkoutDto, {
        new: true,
      })
      .lean()
      .catch(() => null);
  }

  async remove(id: string): Promise<WorkoutInterface> | null {
    const workout = await this.findOne(id);
    if (!workout) {
      return null;
    } else {
      await this.workoutModel.deleteOne({ _id: id });
      return workout;
    }
  }
}
