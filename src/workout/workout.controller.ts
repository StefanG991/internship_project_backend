import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  Query,
  UseGuards,
} from '@nestjs/common';
import { WorkoutService } from './workout.service';
import { CreateWorkoutDto } from './dto/create-workout.dto';
import { UpdateWorkoutDto } from './dto/update-workout.dto';
import { WorkoutInterface } from './interfaces/workout.interface';
import { FilterPaginationParams } from './dto/filterPaginationParams';
import { JwtGuard } from 'src/auth/guards/jwt.guard';

@Controller('workout')
@UseGuards(JwtGuard)
export class WorkoutController {
  constructor(private readonly workoutService: WorkoutService) {}

  @Post()
  create(
    @Body() createWorkoutDto: CreateWorkoutDto,
  ): Promise<WorkoutInterface> {
    return this.workoutService.create(createWorkoutDto);
  }

  @Get()
  findAll(): Promise<WorkoutInterface[]> {
    return this.workoutService.findAll();
  }

  @Get(':id')
  async findOne(
    @Query()
    {
      skip,
      limit,
      exerciseType,
      muscleGroups,
      start,
      end,
    }: FilterPaginationParams,
    @Res() response,
    @Param('id') id: string,
  ): Promise<WorkoutInterface[]> | null {
    const workouts = await this.workoutService.findByUser(
      id,
      skip,
      limit,
      exerciseType,
      muscleGroups,
      start,
      end,
    );
    if (workouts) {
      return response.status(HttpStatus.OK).json({ ...workouts });
    } else {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'User with this id does not have any workouts' });
    }
  }

  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateWorkoutDto: UpdateWorkoutDto,
  ): Promise<WorkoutInterface> | null {
    const workout = await this.workoutService.update(id, updateWorkoutDto);
    if (!workout) {
      return response.status(HttpStatus.NOT_FOUND).json({
        message: 'User with this id does not have any workouts',
      });
    } else {
      return response.status(HttpStatus.OK).json({ ...workout });
    }
  }

  @Delete(':id')
  async remove(@Res() response, @Param('id') id: string): Promise<any> {
    const workout = await this.workoutService.remove(id);
    if (!workout) {
      return response.status(HttpStatus.NOT_FOUND).json({
        message: 'User with this id does not have any workouts',
      });
    } else {
      return response.status(HttpStatus.NO_CONTENT).json({});
    }
  }
}
