import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
} from '@nestjs/common';
import { ExercisesService } from './exercise.service';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';
import { ExerciseInterface } from './interfaces/exercise.interface';
import { response } from 'express';

@Controller('exercises')
export class ExercisesController {
  constructor(private readonly exercisesService: ExercisesService) {}

  @Post()
  async create(
    @Body() createExerciseDto: CreateExerciseDto,
  ): Promise<ExerciseInterface> {
    return this.exercisesService.create(createExerciseDto);
  }

  @Get()
  async findAll(): Promise<ExerciseInterface[]> {
    return this.exercisesService.findAll();
  }

  @Get(':id')
  async findOne(@Res() response, @Param('id') id: string) {
    const exercise = await this.exercisesService.findById(id);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.OK).json({ exercise });
    }
  }

  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateExerciseDto: UpdateExerciseDto,
  ): Promise<ExerciseInterface> | null {
    const exercise = await this.exercisesService.update(id, updateExerciseDto);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.OK).json({ ...exercise });
    }
  }

  @Delete(':id')
  async remove(@Res() response, @Param('id') id: string): Promise<any> {
    const exercise = await this.exercisesService.remove(id);
    if (!exercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise with this id does not exist' });
    } else {
      return response.status(HttpStatus.NO_CONTENT).json({});
    }
  }
}
