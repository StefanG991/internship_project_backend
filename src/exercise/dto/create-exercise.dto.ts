import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class CreateExerciseDto {
  @IsString()
  exerciseType: string;
  @IsBoolean()
  isWeightLifting: boolean;
  @IsNumber()
  series: number;
  @IsOptional()
  @IsArray()
  repetitions: number[];
  @IsOptional()
  @IsArray()
  weight: number[];
  @IsOptional()
  @IsArray()
  time: number[];
  @IsOptional()
  @IsArray()
  distance: number[];
}
