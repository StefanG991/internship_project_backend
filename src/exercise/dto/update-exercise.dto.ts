import {
  IsArray,
  IsBoolean,
  IsNumber,
  IsOptional,
  IsString,
} from 'class-validator';

export class UpdateExerciseDto {
  @IsOptional()
  @IsString()
  exerciseType: string;
  @IsOptional()
  @IsBoolean()
  isWeightLifting: boolean;
  @IsOptional()
  @IsNumber()
  series: number;
  @IsOptional()
  @IsArray()
  repetitions: number[];
  @IsOptional()
  @IsArray()
  weight: number[];
  @IsOptional()
  @IsArray()
  time: number[];
  @IsOptional()
  @IsArray()
  distance: number[];
}
