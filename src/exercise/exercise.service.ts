import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExerciseDto } from './dto/create-exercise.dto';
import { UpdateExerciseDto } from './dto/update-exercise.dto';
import { ExerciseInterface } from './interfaces/exercise.interface';
import { Exercise, ExerciseDocument } from './schemas/exercise.schema';

@Injectable()
export class ExercisesService {
  constructor(
    @InjectModel(Exercise.name)
    private readonly exerciseModel: Model<ExerciseDocument>,
  ) {}

  async create(
    createExerciseDto: CreateExerciseDto,
  ): Promise<ExerciseInterface> {
    const exercise = new this.exerciseModel({ ...createExerciseDto });
    return exercise.save();
  }

  async findAll(): Promise<ExerciseInterface[]> {
    return this.exerciseModel.find().lean();
  }

  async findById(id: string): Promise<ExerciseInterface> | null {
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
      const exercise = await this.exerciseModel.findById(id).lean();
      if (!exercise) {
        return null;
      } else {
        return exercise;
      }
    } else {
      return null;
    }
  }

  update(
    id: string,
    updateExerciseDto: UpdateExerciseDto,
  ): Promise<ExerciseInterface> | null {
    return this.exerciseModel
      .findByIdAndUpdate(id, updateExerciseDto, {
        new: true,
      })
      .lean()
      .catch(() => {
        return null;
      });
  }

  async remove(id: string): Promise<ExerciseInterface> | null {
    const exercise = await this.findById(id);
    if (!exercise) {
      return null;
    } else {
      await this.exerciseModel.deleteOne({ _id: id });
      return exercise;
    }
  }
}
