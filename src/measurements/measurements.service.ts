import {
  BadRequestException,
  ConflictException,
  Injectable,
  UseGuards,
} from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { UploadApiErrorResponse, UploadApiResponse } from 'cloudinary';
import mongoose, { Model } from 'mongoose';
import { CloudinaryService } from 'src/cloudinary/cloudinary.service';
import { CreateMeasurementDto } from './dto/create-measurement.dto';
import { UpdateMeasurementDto } from './dto/update-measurement.dto';
import { MeasurementPaginationInterface } from './interfaces/measurement-pagination.interface';
import { MeasurementInterface } from './interfaces/measurement.interface';
import { Measurement, MeasurementDocument } from './schemas/measurement.schema';

@Injectable()
export class MeasurementsService {
  constructor(
    @InjectModel(Measurement.name)
    private readonly measurementModel: Model<MeasurementDocument>,
    private cloudinary: CloudinaryService,
  ) {}

  async uploadImageToCloudinary(
    file: Express.Multer.File,
  ): Promise<UploadApiResponse | UploadApiErrorResponse> {
    return await this.cloudinary.uploadImage(file).catch(() => {
      throw new BadRequestException('Invalid file type.');
    });
  }

  async delteImageFromCloudinary(name: string): Promise<{ result: string }> {
    return this.cloudinary.deleteImage(name);
  }

  async create(
    createMeasurementDto: CreateMeasurementDto,
  ): Promise<MeasurementInterface> {
    const { date, user } = createMeasurementDto;
    const alreadyExists = await this.measurementModel
      .findOne({
        date,
        user,
      })
      .lean();

    if (alreadyExists) {
      throw new ConflictException(
        'Measurement on this date is already created',
      );
    }

    const newMeasurement = new this.measurementModel({
      ...createMeasurementDto,
    });
    return newMeasurement.save();
  }

  async findAll(): Promise<MeasurementInterface[]> {
    return this.measurementModel.find().lean();
  }

  async findByUser(
    userId: string,
    skip = 1,
    limit: number,
    start?: string,
    end?: string,
  ): Promise<MeasurementPaginationInterface> | null {
    if (userId.match(/^[0-9a-fA-F]{24}$/)) {
      start = start ? start : '2013-10-01T00:00:00.000Z';
      if (end) {
        const arrayFromEnd = end.split('');
        arrayFromEnd.splice(18, 0, '9');
        end = arrayFromEnd.join('');
      }
      let measurements;
      if (limit) {
        skip = skip < 1 ? 1 : skip;
        limit = limit > 50 ? 50 : limit;
        measurements = await this.measurementModel.aggregate([
          {
            $match: {
              $and: [
                { user: new mongoose.Types.ObjectId(userId) },
                {
                  date: {
                    $gte: new Date(start),
                    $lt: end ? new Date(end) : new Date(),
                  },
                },
              ],
            },
          },

          { $sort: { date: -1 } },
          {
            $facet: {
              measurements: [{ $skip: (skip - 1) * limit }, { $limit: limit }],
              totalCount: [
                {
                  $count: 'count',
                },
              ],
            },
          },
        ]);
        return {
          measurements: measurements[0].measurements,
          numOfMeasurements: measurements[0].totalCount[0]?.count,
        };
      } else {
        measurements = await this.measurementModel.aggregate([
          {
            $match: {
              $and: [
                { user: new mongoose.Types.ObjectId(userId) },
                {
                  date: {
                    $gte: new Date(start),
                    $lt: end ? new Date(end) : new Date(),
                  },
                },
              ],
            },
          },

          { $sort: { date: 1 } },
          {
            $facet: {
              measurements: [{ $skip: 0 }],
              totalCount: [
                {
                  $count: 'count',
                },
              ],
            },
          },
        ]);
        return {
          measurements: measurements[0].measurements,
          numOfMeasurements: measurements[0].totalCount[0]?.count,
        };
      }
    } else {
      return null;
    }
  }

  async findById(id: string): Promise<MeasurementInterface> | null {
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
      const measurement = await this.measurementModel.findById(id).lean();
      if (!measurement) return null;
      return measurement;
    } else {
      return null;
    }
  }

  update(
    id: string,
    updateMeasurementDto: UpdateMeasurementDto,
  ): Promise<MeasurementInterface> | null {
    return this.measurementModel
      .findByIdAndUpdate(id, updateMeasurementDto, {
        new: true,
      })
      .lean()
      .catch(() => null);
  }

  async delete(id: string): Promise<MeasurementInterface> | null {
    const existingMeasurement = await this.findById(id);
    if (!existingMeasurement) {
      return null;
    } else {
      await this.measurementModel.deleteOne({ _id: id });
      return existingMeasurement;
    }
  }
}
