import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import mongoose, { Document } from 'mongoose';
import { User } from 'src/users/schemas/user.schema';

export type MeasurementDocument = Measurement & Document;

@Schema()
export class Measurement {
  @Prop({ required: true })
  photoUrl: string;
  @Prop({ required: true })
  photoId: string;
  @Prop({ required: true })
  weight: number;
  @Prop({ required: true })
  chest: number;
  @Prop({ required: true })
  waist: number;
  @Prop({ required: true })
  hips: number;
  @Prop({ required: true })
  biceps: number;
  @Prop({ required: true })
  date: Date;
  @Prop({ required: true, type: mongoose.Schema.Types.ObjectId, ref: 'User' })
  user: User;
}

export const MeasurementSchema = SchemaFactory.createForClass(Measurement);
