import { IsDateString, IsNumber, IsOptional, IsString } from 'class-validator';
export class UpdateMeasurementDto {
  @IsOptional()
  @IsString()
  photoUrl: string;
  @IsOptional()
  @IsString()
  photoId: string;
  @IsOptional()
  @IsNumber()
  weight: number;
  @IsOptional()
  @IsNumber()
  chest: number;
  @IsOptional()
  @IsNumber()
  waist: number;
  @IsOptional()
  @IsNumber()
  hips: number;
  @IsOptional()
  @IsNumber()
  biceps: number;
  @IsOptional()
  @IsDateString()
  date: Date;
}
