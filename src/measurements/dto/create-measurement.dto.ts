import { IsDateString, IsNumber, IsString } from 'class-validator';
export class CreateMeasurementDto {
  @IsString()
  photoUrl: string;
  @IsString()
  photoId: string;
  @IsNumber()
  weight: number;
  @IsNumber()
  chest: number;
  @IsNumber()
  waist: number;
  @IsNumber()
  hips: number;
  @IsNumber()
  biceps: number;
  @IsDateString()
  date: Date;
  @IsString()
  user: string;
}
