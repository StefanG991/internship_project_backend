import { User } from 'src/users/schemas/user.schema';

export interface MeasurementInterface {
  photoId: string;
  photoUrl: string;
  _id: string;
  weight: number;
  chest: number;
  waist: number;
  hips: number;
  biceps: number;
  date: Date;
  user: User;
}
