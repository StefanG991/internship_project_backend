import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  UploadedFile,
  UseInterceptors,
  Query,
  UseGuards,
} from '@nestjs/common';
import { MeasurementsService } from './measurements.service';
import { CreateMeasurementDto } from './dto/create-measurement.dto';
import { UpdateMeasurementDto } from './dto/update-measurement.dto';
import { MeasurementInterface } from './interfaces/measurement.interface';
import { FileInterceptor } from '@nestjs/platform-express';
import { UploadApiErrorResponse, UploadApiResponse } from 'cloudinary';
import { PaginationParams } from './dto/paginationParams';
import { JwtGuard } from 'src/auth/guards/jwt.guard';

@Controller('measurements')
@UseGuards(JwtGuard)
export class MeasurementsController {
  constructor(private readonly measurementsService: MeasurementsService) {}

  @Post('upload')
  @UseInterceptors(FileInterceptor('file'))
  async upload(
    @UploadedFile() file: Express.Multer.File,
  ): Promise<UploadApiResponse | UploadApiErrorResponse> {
    return this.measurementsService.uploadImageToCloudinary(file);
  }

  @Delete('delete/:id')
  async deleteImage(@Param('id') id: string): Promise<{ result: string }> {
    return this.measurementsService.delteImageFromCloudinary(id);
  }

  @Post()
  async create(
    @Body() createMeasurementDto: CreateMeasurementDto,
  ): Promise<MeasurementInterface> {
    return this.measurementsService.create(createMeasurementDto);
  }

  @Get()
  async findAll(): Promise<MeasurementInterface[]> {
    return this.measurementsService.findAll();
  }

  @Get(':id')
  async findOne(
    @Query() { skip, limit, start, end }: PaginationParams,
    @Res() response,
    @Param('id') id: string,
  ): Promise<MeasurementInterface[]> {
    const measurements = await this.measurementsService.findByUser(
      id,
      skip,
      limit,
      start,
      end,
    );
    if (measurements) {
      return response.status(HttpStatus.OK).json(measurements);
    } else {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'User with this id does not have any measurements' });
    }
  }

  @Patch(':id')
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateMeasurementDto: UpdateMeasurementDto,
  ): Promise<MeasurementInterface> {
    const updatedMeasurement = await this.measurementsService.update(
      id,
      updateMeasurementDto,
    );
    if (!updatedMeasurement) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'There is no measurement with this id' });
    } else {
      return response.status(HttpStatus.OK).json({
        ...updatedMeasurement,
      });
    }
  }

  @Delete(':id')
  async delete(
    @Res() response,
    @Param('id') id: string,
  ): Promise<MeasurementInterface> {
    const measurement = await this.measurementsService.delete(id);
    if (!measurement) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'There is no measurement with this id' });
    } else {
      return response.status(HttpStatus.NO_CONTENT).json({});
    }
  }
}
