import {
  BadRequestException,
  ConflictException,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { ChangePasswordDto } from 'src/auth/dto/change-password.dto';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { ExistingUserDto } from 'src/users/dtos/existing-user.dto';
import { UserInterface } from 'src/users/interfaces/user.interface';
import { UserDocument } from 'src/users/schemas/user.schema';
import { UsersService } from 'src/users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly userService: UsersService,
    private jwtService: JwtService,
  ) {}

  async hashPassword(password: string): Promise<string> {
    return bcrypt.hash(password, 12);
  }

  async register(user: CreateUserDto): Promise<UserInterface> {
    const existingUser = await this.userService.findByEmail(user.email);
    if (existingUser) throw new ConflictException('Email taken');
    user.password = await this.hashPassword(user.password);
    const newUser = await this.userService.create(user);
    return newUser;
  }

  async doesPasswordMatch(
    password: string,
    hashedPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(password, hashedPassword);
  }

  async validateUser(
    email: string,
    password: string,
  ): Promise<UserDocument | null> {
    const user = await this.userService.findByEmail(email);
    const doesUserExist = !!user;
    if (!doesUserExist) return null;
    const doesPasswordMatch = await this.doesPasswordMatch(
      password,
      user.password,
    );
    if (!doesPasswordMatch) return null;
    return user;
  }

  async login(existingUser: ExistingUserDto): Promise<{ token: string }> {
    const { email, password } = existingUser;
    const user = await this.validateUser(email, password);
    if (!user) throw new UnauthorizedException();
    const jwt = await this.jwtService.signAsync({ user });
    return { token: jwt };
  }

  async changePass(changePassDto: ChangePasswordDto) {
    const { email, oldPassword, newPassword, confirmedNewPassword } =
      changePassDto;

    if (newPassword !== confirmedNewPassword) {
      throw new BadRequestException('Passwords do not match');
    }
    const user = await this.validateUser(email, oldPassword);
    if (!user) {
      throw new BadRequestException('Incorrect email or old password');
    } else {
      user.password = await this.hashPassword(newPassword);
      return user.save();
    }
  }
}
