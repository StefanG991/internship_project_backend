import {
  Body,
  Controller,
  HttpCode,
  HttpStatus,
  Patch,
  Post,
} from '@nestjs/common';
import { ChangePasswordDto } from 'src/auth/dto/change-password.dto';
import { CreateUserDto } from 'src/users/dtos/create-user.dto';
import { ExistingUserDto } from 'src/users/dtos/existing-user.dto';
import { UserInterface } from 'src/users/interfaces/user.interface';
import { UserDocument } from 'src/users/schemas/user.schema';
import { UsersService } from 'src/users/users.service';
import { AuthService } from './auth.service';

@Controller('auth')
export class AuthController {
  constructor(
    private readonly authService: AuthService,
    private readonly userService: UsersService,
  ) {}

  @Post('register')
  register(@Body() user: CreateUserDto): Promise<UserInterface> {
    return this.authService.register(user);
  }

  @Post('login')
  @HttpCode(HttpStatus.OK)
  login(@Body() user: ExistingUserDto): Promise<{ token: string }> {
    return this.authService.login(user);
  }

  @Patch('password/:id')
  async changePass(
    @Body()
    changePasswordDto: ChangePasswordDto,
  ): Promise<UserDocument> {
    return await this.authService.changePass(changePasswordDto);
  }
}
