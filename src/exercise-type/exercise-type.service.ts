import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { CreateExerciseTypeDto } from './dto/create-exercise-type.dto';
import { UpdateExerciseTypeDto } from './dto/update-exercise-type.dto';
import { ExerciseTypeInterface } from './interfaces/exercise-type.interface';

import {
  ExerciseType,
  ExerciseTypeDocument,
} from './schemas/exercise-type.schema';

@Injectable()
export class ExerciseTypeService {
  constructor(
    @InjectModel(ExerciseType.name)
    private readonly exerciseTypeModel: Model<ExerciseTypeDocument>,
  ) {}

  async create(
    createExerciseTypeDto: CreateExerciseTypeDto,
  ): Promise<ExerciseTypeInterface> {
    const newExercise = new this.exerciseTypeModel({
      ...createExerciseTypeDto,
    });
    return newExercise.save();
  }

  async findAll(): Promise<ExerciseTypeInterface[]> {
    return this.exerciseTypeModel.find().lean();
  }

  async findOne(id: string): Promise<ExerciseTypeInterface> | null {
    if (id.match(/^[0-9a-fA-F]{24}$/)) {
      const exercise = await this.exerciseTypeModel.findById(id).lean();
      if (!exercise) return null;
      return exercise;
    } else {
      return null;
    }
  }

  update(
    id: string,
    updateExercise: UpdateExerciseTypeDto,
  ): Promise<ExerciseTypeInterface> | null {
    return this.exerciseTypeModel
      .findByIdAndUpdate(id, updateExercise, {
        new: true,
      })
      .lean()
      .catch(() => {
        return null;
      });
  }

  async remove(id: string): Promise<ExerciseTypeInterface> | null {
    const existingExercise = await this.findOne(id);
    if (!existingExercise) {
      return null;
    } else {
      await this.exerciseTypeModel.deleteOne({ _id: id });
      return existingExercise;
    }
  }
}
