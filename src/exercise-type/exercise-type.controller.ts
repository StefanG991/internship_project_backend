import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  Res,
  HttpStatus,
  UseGuards,
} from '@nestjs/common';
import { ExerciseTypeService } from './exercise-type.service';
import { CreateExerciseTypeDto } from './dto/create-exercise-type.dto';
import { UpdateExerciseTypeDto } from './dto/update-exercise-type.dto';
import { ExerciseTypeInterface } from './interfaces/exercise-type.interface';
import { Roles } from './roles.decorator';
import { Role } from 'src/users/interfaces/role.enum';
import { JwtGuard } from 'src/auth/guards/jwt.guard';
import { RolesGuard } from './roles.guard';

@Controller('exercise-type')
@UseGuards(JwtGuard)
export class ExerciseTypeController {
  constructor(private readonly exerciseTypeService: ExerciseTypeService) {}

  @Post()
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  async create(
    @Body() createExerciseTypeDto: CreateExerciseTypeDto,
  ): Promise<ExerciseTypeInterface> {
    const newExercise = await this.exerciseTypeService.create(
      createExerciseTypeDto,
    );
    return newExercise;
  }

  @Get()
  async findAll(@Res() response): Promise<ExerciseTypeInterface[]> {
    const exercises = await this.exerciseTypeService.findAll();
    return response.status(HttpStatus.OK).json([...exercises]);
  }

  @Get(':id')
  async findOne(
    @Res() response,
    @Param('id') id: string,
  ): Promise<ExerciseTypeInterface> {
    const exercise = await this.exerciseTypeService.findOne(id);
    if (exercise) {
      return response.status(HttpStatus.OK).json({
        exercise,
      });
    } else {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise type with id does not exist' });
    }
  }

  @Patch(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  async update(
    @Res() response,
    @Param('id') id: string,
    @Body() updateExerciseTypeDto: UpdateExerciseTypeDto,
  ): Promise<ExerciseTypeInterface> {
    const newExercise = await this.exerciseTypeService.update(
      id,
      updateExerciseTypeDto,
    );
    if (!newExercise) {
      return response
        .status(HttpStatus.NOT_FOUND)
        .json({ message: 'Exercise type with id does not exist' });
    } else {
      return response.status(HttpStatus.OK).json({
        ...newExercise,
      });
    }
  }

  @Delete(':id')
  @UseGuards(RolesGuard)
  @Roles(Role.Admin)
  async remove(
    @Res() response,
    @Param('id') id: string,
  ): Promise<ExerciseTypeInterface> {
    const existingExercise = await this.exerciseTypeService.remove(id);
    if (existingExercise !== null) {
      return response.status(HttpStatus.NO_CONTENT).json({});
    } else {
      return response.status(HttpStatus.NOT_FOUND).json({
        message: 'User with id does not exist',
      });
    }
  }
}
