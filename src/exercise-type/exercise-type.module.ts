import { Module } from '@nestjs/common';
import { ExerciseTypeService } from './exercise-type.service';
import { ExerciseTypeController } from './exercise-type.controller';
import { MongooseModule } from '@nestjs/mongoose';
import {
  ExerciseType,
  ExerciseTypeSchema,
} from './schemas/exercise-type.schema';

@Module({
  imports: [
    MongooseModule.forFeature([
      { name: ExerciseType.name, schema: ExerciseTypeSchema },
    ]),
  ],
  controllers: [ExerciseTypeController],
  providers: [ExerciseTypeService],
})
export class ExerciseTypeModule {}
